﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CompanyX_WPF
{
    /// <summary>
    /// Interaction logic for MainUI.xaml
    /// </summary>
    public partial class MainUI : Window
    {
        CompanyXEntities CompX = new CompanyXEntities();
        public string UserId;
        public class Textblock
        { 
            public string TextData { get; set; }
        }
        public MainUI()
        {
            InitializeComponent();
        }
        public MainUI(string id)
        {
            InitializeComponent();
            UserId = id;
            //WelcomeBox.DataContext = new Textblock() { TextData = CompX.Employees.First(x => x.IdCode == id).FullName };           
        }

        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow newLogin = new LoginWindow();
            MessageBoxResult result = MessageBox.Show("Are you sure?", "Log Out", MessageBoxButton.YesNo, MessageBoxImage.Question,MessageBoxResult.Yes);
            if (result == MessageBoxResult.Yes)
            {
                newLogin.Show();
                Close();
            }
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Exit the Program?", "Quit", MessageBoxButton.OKCancel, MessageBoxImage.Warning, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                Close();
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            AddCourseWindow newEvent = new AddCourseWindow(UserId);
            newEvent.Show();
        }
    }
}
