﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CompanyX_WPF
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        CompanyXEntities CompX = new CompanyXEntities();
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            var isik = CompX.Employees.Where(x => x.IdCode == LoginTextbox.Text).ToList();
            if (isik.Count > 0)
            {
                //MessageBox.Show($"Welcome, {isik[0].FullName}!","Success");
                MainUI main = new MainUI(LoginTextbox.Text);
                main.Show();
                Close();
            }
            else
            {
                MessageBox.Show("Wrong ID", "Error");
                LoginTextbox.Text = "";
            }
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void LoginTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var isik = CompX.Employees.Where(x => x.IdCode == LoginTextbox.Text).ToList();
                if (isik.Count > 0)
                {
                    //MessageBox.Show($"Welcome, {isik[0].FullName}!","Success");
                    MainUI main = new MainUI(LoginTextbox.Text);
                    main.Show();
                    Close();
                }
                else
                {
                    MessageBox.Show("Wrong ID", "Error");
                    LoginTextbox.Text = "";
                }
            }
        }
    }
}
